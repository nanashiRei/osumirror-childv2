<?php

class OsuMirror_Application
{
    protected static $_instance;
    
    protected $_config;
    protected $_route;
    protected $_controller;
    protected $_statistics;
    
    public function __construct() 
    {
        // Make sure we have the AutoLoader
        require_once APPLICATION_PATH.'/library/AutoLoader.php';
        OsuMirror_AutoLoader::getInstance()
            ->registerAutoLoader();
        // Install the ErrorHandler
        //OsuMirror_ErrorHandler::getInstance()
            //->register();
        // Load the Configuration
        $this->_config = OsuMirror_Config::getInstance();
        // Get the default route
        $this->_route = OsuMirror_Route::getInstance();
        // Load the statistics class
        $this->_statistics = OsuMirror_Statistics::getInstance(); 
    }
    
    public static function getInstance()
    {
        if(null === self::$_instance)
            self::$_instance = new self;
        return self::$_instance;
    }
    
    public function run()
    {
        // Try loading the controller for the default route
        try {
            $controllerName = 'Controller_' . ucfirst($this->_route->getController());
            $this->_controller = new $controllerName;
        } catch(Exception $exception) {
            throw new Exception('A controller for ' . $this->_route->getController() . ' could not be found.',0,$exception);
        }
        
        // Prepare the controller
        try {
            $this->_controller->setRoute($this->_route)
                ->setConfig($this->_config)
                ->setStats($this->_statistics)
                ->init();
        } catch(Exception $exception) {
            throw new Exception('Controller initialization has failed.',$exception->getCode(),$exception);
        }
        
        // Try calling the current action
        $actionName = $this->_route->getAction() . 'Action';
        if (method_exists($this->_controller, $actionName)) {
            $this->_controller->$actionName();
        } else {
            throw new Exception(get_class($this->_controller).'::'.$actionName.' could not be called.');
        }
        
        // Call the render function of the controller
        try {
            $this->_controller->render();
        } catch(Exception $exception) {
            throw new Exception('Could not render the controller.',0,$exception);
        }
    }
    
	/**
     * @return the $_config
     */
    public function getConfig ()
    {
        return $this->_config;
    }

	/**
     * @return the $_route
     */
    public function getRoute ()
    {
        return $this->_route;
    }

	/**
     * @return the $_controller
     */
    public function getController ()
    {
        return $this->_controller;
    }

	/**
     * @return the $_statistics
     */
    public function getStatistics ()
    {
        return $this->_statistics;
    }

	/**
     * @param OsuMirror_Config $_config
     */
    public function setConfig ($_config)
    {
        $this->_config = $_config;
        return $this;
    }

	/**
     * @param OsuMirror_Route $_route
     */
    public function setRoute ($_route)
    {
        $this->_route = $_route;
        return $this;
    }

	/**
     * @param field_type $_controller
     */
    public function setController ($_controller)
    {
        $this->_controller = $_controller;
        return $this;
    }

	/**
     * @param OsuMirror_Statistics $_statistics
     */
    public function setStatistics ($_statistics)
    {
        $this->_statistics = $_statistics;
        return $this;
    }

}