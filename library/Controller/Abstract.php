<?php

abstract class OsuMirror_Controller_Abstract
{
    protected $_route;
    protected $_config;
    protected $_encryption;
    protected $_stats;
    
    protected $_render = true;
    
    public $view;
    
    public function __construct()
    {
        $app = OsuMirror_Application::getInstance();
        $this->_config = $app->getConfig();
        $this->_encryption = OsuMirror_Encryption::getInstance();
        $this->_stats = OsuMirror_Statistics::getInstance();
        
        $this->view = new OsuMirror_View();
        $this->init();
    }
    
    abstract public function init(); 
    
    public function render()
    {
        if($this->_render === true) {
            $this->view->dispatch();
        }
    }
    
    public function __set($name,$value)
    {
        $func = 'set'.ucfirst($name);
        if (method_exists($this,$func)) {
            return $this->$func($value);
        }
        return false;
    }
    
    public function __get($name)
    {
        $func = 'get'.ucfirst($name);
        if (method_exists($this, $func)) {
            return $this->$func();
        }
    }
    
	/**
     * @return the $_route
     */
    public function getRoute ()
    {
        return $this->_route;
    }

	/**
     * @param field_type $_route
     */
    public function setRoute ($_route)
    {
        $this->_route = $_route;
        return $this;
    }

	/**
     * @return the $_config
     */
    public function getConfig ()
    {
        return $this->_config;
    }

	/**
     * @param field_type $_config
     */
    public function setConfig ($_config)
    {
        $this->_config = $_config;
        return $this;
    }

	/**
     * @return the $_encryption
     */
    public function getEncryption ()
    {
        return $this->_encryption;
    }

	/**
     * @param field_type $_encryption
     */
    public function setEncryption ($_encryption)
    {
        $this->_encryption = $_encryption;
        return $this;
    }
	/**
     * @return the $_stats
     */
    public function getStats ()
    {
        return $this->_stats;
    }

	/**
     * @param field_type $_stats
     */
    public function setStats ($_stats)
    {
        $this->_stats = $_stats;
        return $this;
    }
	/**
     * @return the $_render
     */
    public function getRender ()
    {
        return $this->_render;
    }

	/**
     * @return the $view
     */
    public function getView ()
    {
        return $this->view;
    }

	/**
     * @param boolean $_render
     */
    public function setRender ($_render)
    {
        $this->_render = $_render;
        return $this;
    }

	/**
     * @param OsuMirror_View $view
     */
    public function setView ($view)
    {
        $this->view = $view;
        return $this;
    }



}

?>