<?php

class OsuMirror_ErrorHandler
{
    protected static $_instance;
    
    public function register()
    {
        set_error_handler(array(get_class($this),'errorHandler'));
        set_exception_handler(array(get_class($this),'exceptionHandler'));
    }
    
    public static function getInstance()
    {
        if(null === self::$_instance)
            self::$_instance = new self;
        return self::$_instance;
    }
    
    public static function exceptionHandler(Exception $e) 
    {
        $app = OsuMirror_Application::getInstance();
        $app->setRoute(new OsuMirror_Route('error/index'))
            ->setController(new Controller_Error());
        $app->getController()->setException($e);
        $app->run();
    }

    public static function errorHandler($no,$str,$file,$line) 
    {
        $e = new ErrorException($str,$no,0,$file,$line);
        self::exceptionHandler($e);
    }
}