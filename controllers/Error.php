<?php

class Controller_Error extends OsuMirror_Controller_Abstract
{
	/* (non-PHPdoc)
     * @see OsuMirror_Controller_Abstract::_init()
     */
    protected $_exception = null;
    
    public function init ()
    {
        
    }
    
    public function indexAction()
    {
        
    }
    
    public function getException()
    {
        return $this->_exception;
    }
    
    public function setException($exception)
    {
        $this->_exception = $exception;
        return $this;
    }
}