<?php

ini_set('display_errors',true);
error_reporting(E_ALL & ~E_NOTICE);
date_default_timezone_set('Europe/Berlin');

define('APPLICATION_PATH', realpath(dirname(__FILE__).'/..'));

require_once APPLICATION_PATH . '/library/Application.php';
$app = new OsuMirror_Application();
$app->run();