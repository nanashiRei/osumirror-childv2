function loadStats(){
    $('#stats').html('<div style="text-align: center;"><img src="/img/child-load.gif" alt="loading" style="vertical-align: top;" /></div>');
    setInterval("$('#stats').load('/index/stats');",3500);
}
$(document).ready(function(){
    $('#load-stats').click(loadStats);
    $.get('/api/version',{},function(data){
        var dataMatch = data.match(/^([a-z]+)\s+\"([^\"]+)\"/i);
        $('#version').text(dataMatch[2]);
    },'html');
    
});